package ru.tsc.avramenko.tm;

import ru.tsc.avramenko.tm.bootstrap.Bootstrap;

public class Application
{
    public static void main( String[] args )
    {
        final Bootstrap bootstrap = new Bootstrap();
        bootstrap.run();
    }
}