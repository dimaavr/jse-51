package ru.tsc.avramenko.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.avramenko.tm.endpoint.Role;
import ru.tsc.avramenko.tm.endpoint.Session;
import ru.tsc.avramenko.tm.endpoint.SessionDTO;
import ru.tsc.avramenko.tm.exception.system.AccessDeniedException;
import java.util.Optional;

public class BackupSaveCommand extends AbstractDataCommand {

    @NotNull
    public final static String BACKUP_SAVE = "backup-save";

    @NotNull
    @Override
    public String name() {
        return BACKUP_SAVE;
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Backup data.";
    }

    @Override
    @SneakyThrows
    public void execute() {
        @Nullable final SessionDTO session = serviceLocator.getSessionService().getSession();
        Optional.ofNullable(session).orElseThrow(AccessDeniedException::new);
        serviceLocator.getAdminDataEndpoint().saveBackup(session);
    }

    @Nullable
    public Role[] roles() {
        if (serviceLocator.getSessionService().getSession() == null || Thread.currentThread().getName().equals("DataThread")) {
            return null;
        }
        else {
            return new Role[]{Role.ADMIN};
        }
    }

}