package ru.tsc.avramenko.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.avramenko.tm.endpoint.Role;
import ru.tsc.avramenko.tm.endpoint.Session;
import ru.tsc.avramenko.tm.endpoint.SessionDTO;
import ru.tsc.avramenko.tm.exception.system.AccessDeniedException;
import java.util.Optional;

public final class DataXmlSaveFasterXmlCommand extends AbstractDataCommand {

    @NotNull
    @Override
    public String name() {
        return "data-xml-save";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Save data to xml file (Faster XML).";
    }

    @Override
    @SneakyThrows
    public void execute() {
        @Nullable final SessionDTO session = serviceLocator.getSessionService().getSession();
        Optional.ofNullable(session).orElseThrow(AccessDeniedException::new);
        serviceLocator.getAdminDataEndpoint().saveDataXml(session);
    }

    @NotNull
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}