package ru.tsc.avramenko.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import ru.tsc.avramenko.tm.component.Bootstrap;
import ru.tsc.avramenko.tm.enumerated.Role;
import ru.tsc.avramenko.tm.exception.empty.EmptyIdException;
import ru.tsc.avramenko.tm.exception.empty.EmptyLoginException;
import ru.tsc.avramenko.tm.exception.empty.EmptyPasswordException;
import ru.tsc.avramenko.tm.exception.entity.UserNotFoundException;
import ru.tsc.avramenko.tm.dto.SessionDTO;
import ru.tsc.avramenko.tm.dto.UserDTO;
import ru.tsc.avramenko.tm.model.Session;
import ru.tsc.avramenko.tm.model.User;
import ru.tsc.avramenko.tm.service.dto.SessionDtoService;
import ru.tsc.avramenko.tm.service.dto.UserDtoService;
import ru.tsc.avramenko.tm.service.model.SessionService;
import ru.tsc.avramenko.tm.service.model.UserService;

import java.util.List;

public class UserServiceTest {

    @Nullable
    private static UserService userService;

    @Nullable
    private User user;

    @Nullable
    private SessionService sessionService;

    @NotNull
    private Bootstrap bootstrap = new Bootstrap();

    @Nullable
    private Session session;

    @NotNull
    protected static final String TEST_USER_LOGIN = "TestLogin";

    @NotNull
    protected static final String TEST_USER_EMAIL = "TestEmail";

    @BeforeClass
    public static void beforeClass() {
        ConnectionService connectionService = new ConnectionService(new PropertyService());
        userService = new UserService(connectionService, new PropertyService());
        userService.create("Admin", "Admin", Role.ADMIN);
    }

    @Before
    public void before() {
        ConnectionService connectionService = new ConnectionService(new PropertyService());
        sessionService = new SessionService(connectionService, bootstrap);
        this.session = sessionService.open("Admin", "Admin");
        userService = new UserService(connectionService, new PropertyService());
        this.user = userService.create(TEST_USER_LOGIN, "12345", TEST_USER_EMAIL);
    }

    @After
    @SneakyThrows
    public void after() {
        userService.removeByLogin(TEST_USER_LOGIN);
        sessionService.close(session);
    }

    @Test
    public void add() {
        Assert.assertNotNull(user);
        Assert.assertNotNull(user.getId());
        Assert.assertNotNull(user.getLogin());
        Assert.assertEquals(TEST_USER_LOGIN, user.getLogin());
        Assert.assertNotNull(user.getEmail());
        Assert.assertEquals(TEST_USER_EMAIL, user.getEmail());

        @Nullable final User userById = userService.findById(user.getId());
        Assert.assertNotNull(userById);
        Assert.assertEquals(user.getId(), userById.getId());
    }

    @Test
    public void findAll() {
        @Nullable final List<User> users = userService.findAll();
        Assert.assertTrue(users.size() > 1);
    }

    @Test
    public void findById() {
        @Nullable final User user = userService.findById(this.user.getId());
        Assert.assertNotNull(user);
    }

    @Test
    public void findByIdIncorrect() {
        @Nullable final User user = userService.findById("647");
        Assert.assertNull(user);
    }

    @Test(expected = EmptyIdException.class)
    public void findByIdNull() {
        @Nullable final User user = userService.findById(null);
        Assert.assertNull(user);
    }

    @Test
    public void findByLogin() {
        @Nullable final User user = userService.findByLogin(this.user.getLogin());
        Assert.assertNotNull(user);
    }

    @Test
    public void findByLoginIncorrect() {
        @Nullable final User user = userService.findByLogin("647");
        Assert.assertNull(user);
    }

    @Test(expected = EmptyLoginException.class)
    public void findByLoginNull() {
        @Nullable final User user = userService.findByLogin(null);
        Assert.assertNull(user);
    }

    @Test
    public void isLoginExist() {
        Assert.assertTrue(userService.isLoginExist(this.user.getLogin()));
    }

    @Test
    public void isLoginExistFalse() {
        Assert.assertFalse(userService.isLoginExist("dcsdcsx"));
    }

    @Test
    public void isEmailExist() {
        Assert.assertTrue(userService.isEmailExist(user.getEmail()));
    }

    @Test
    public void isEmailExistFalse() {
        Assert.assertFalse(userService.isEmailExist("email"));
    }

    @Test
    public void setPassword() {
        @NotNull final User user = userService.setPassword(this.user.getId(), "password");
        Assert.assertNotNull(user);
    }

    @Test(expected = EmptyIdException.class)
    public void setPasswordUserIdNull() {
        @NotNull final User user = userService.setPassword(null, "password");
        Assert.assertNotNull(user);
    }

    @Test(expected = UserNotFoundException.class)
    public void setPasswordUserIdIncorrect() {
        @NotNull final User user = userService.setPassword("null", "password");
        Assert.assertNotNull(user);
    }

    @Test(expected = EmptyPasswordException.class)
    public void setPasswordNull() {
        @NotNull final User user = userService.setPassword("null", null);
        Assert.assertNotNull(user);
    }

    @Test
    public void lockUserByLogin() {
        @NotNull final User user = userService.lockUserByLogin(this.user.getLogin());
        Assert.assertNotNull(user);
        Assert.assertTrue(user.getLocked());
    }

    @Test
    public void unlockUserByLogin() {
        @NotNull final User user = userService.unlockUserByLogin(this.user.getLogin());
        Assert.assertNotNull(user);
        Assert.assertFalse(user.getLocked());
    }

}