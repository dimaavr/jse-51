package ru.tsc.avramenko.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import ru.tsc.avramenko.tm.component.Bootstrap;
import ru.tsc.avramenko.tm.enumerated.Status;
import ru.tsc.avramenko.tm.exception.empty.EmptyIdException;
import ru.tsc.avramenko.tm.exception.empty.EmptyNameException;
import ru.tsc.avramenko.tm.exception.entity.TaskNotFoundException;
import ru.tsc.avramenko.tm.dto.SessionDTO;
import ru.tsc.avramenko.tm.dto.TaskDTO;
import ru.tsc.avramenko.tm.model.Session;
import ru.tsc.avramenko.tm.model.Task;
import ru.tsc.avramenko.tm.service.dto.SessionDtoService;
import ru.tsc.avramenko.tm.service.dto.TaskDtoService;
import ru.tsc.avramenko.tm.service.model.SessionService;
import ru.tsc.avramenko.tm.service.model.TaskService;
import ru.tsc.avramenko.tm.service.model.UserService;

import java.util.List;

public class TaskServiceTest {

    @Nullable
    private TaskService taskService;

    @Nullable
    private static UserService userService;

    @Nullable
    private Task task;

    @Nullable
    private SessionService sessionService;

    @NotNull
    private Bootstrap bootstrap = new Bootstrap();

    @Nullable
    private Session session;

    @NotNull
    protected static final String TEST_TASK_NAME = "TestName";

    @NotNull
    protected static final String TEST_DESCRIPTION_NAME = "TestDescription";

    @NotNull
    protected static final String TEST_USER_ID = "TestUserId";

    @NotNull
    protected static final String TEST_USER_ID_INCORRECT = "TestUserIdIncorrect";

    @NotNull
    protected static final String TEST_TASK_ID_INCORRECT = "647";

    @BeforeClass
    public static void beforeClass() {
        ConnectionService connectionService = new ConnectionService(new PropertyService());
        userService = new UserService(connectionService, new PropertyService());
        userService.create("Test3", "Test3");
    }

    @Before
    public void before() {
        ConnectionService connectionService = new ConnectionService(new PropertyService());
        sessionService = new SessionService(connectionService, bootstrap);
        this.session = sessionService.open("Test3", "Test3");
        taskService = new TaskService(connectionService);
        taskService.create(session.getUser().getId(), TEST_TASK_NAME, TEST_DESCRIPTION_NAME);
        this.task = taskService.findByName(session.getUser().getId(), TEST_TASK_NAME);
    }

    @After
    public void after() {
        taskService.clear(session.getUser().getId());
        sessionService.close(session);
    }

    @Test
    public void add() {
        Assert.assertNotNull(task);
        Assert.assertNotNull(task.getId());
        Assert.assertNotNull(task.getName());
        Assert.assertNotNull(task.getDescription());
        Assert.assertEquals(TEST_TASK_NAME, task.getName());
        Assert.assertEquals(TEST_DESCRIPTION_NAME, task.getDescription());

        @NotNull final Task taskById = taskService.findById(session.getUser().getId(), task.getId());
        Assert.assertNotNull(taskById);
        Assert.assertEquals(task.getId(), taskById.getId());
    }

    @Test
    public void findAllByUserId() {
        @Nullable final List<Task> tasks = taskService.findAll(session.getUser().getId());
        Assert.assertEquals(1, tasks.size());
    }

    @Test
    public void findById() {
        @Nullable final Task task = taskService.findById(session.getUser().getId(), this.task.getId());
        Assert.assertNotNull(task);
    }

    @Test
    public void findByIdIncorrect() {
        @Nullable final Task task = taskService.findById(session.getUser().getId(), TEST_TASK_ID_INCORRECT);
        Assert.assertNull(task);
    }

    @Test(expected = EmptyIdException.class)
    public void findByIdNull() {
        @Nullable final Task task = taskService.findById(session.getUser().getId(), null);
        Assert.assertNull(task);
    }

    @Test
    public void findByIdIncorrectUser() {
        @Nullable final Task task = taskService.findById(TEST_USER_ID_INCORRECT, this.task.getId());
        Assert.assertNull(task);
    }

    @Test
    public void remove() {
        taskService.removeById(session.getUser().getId(), task.getId());
        Assert.assertNull(taskService.findById(session.getUser().getId(), task.getId()));
    }

    @Test
    public void findByName() {
        @Nullable final Task task = taskService.findByName(session.getUser().getId(), TEST_TASK_NAME);
        Assert.assertNotNull(task);
    }

    @Test
    public void findByNameIncorrect() {
        @Nullable final Task task = taskService.findByName(session.getUser().getId(), TEST_TASK_ID_INCORRECT);
        Assert.assertNull(task);
    }

    @Test(expected = EmptyNameException.class)
    public void findByNameNull() {
        @Nullable final Task task = taskService.findByName(session.getUser().getId(), null);
        Assert.assertNull(task);
    }

    @Test
    public void findByNameIncorrectUser() {
        @Nullable final Task task = taskService.findByName(TEST_USER_ID_INCORRECT, this.task.getName());
        Assert.assertNull(task);
    }

    @Test
    public void findByIndex() {
        @NotNull final Task task = taskService.findByIndex(session.getUser().getId(), 0);
        Assert.assertNotNull(task);
    }

    @Test
    public void removeById() {
        taskService.removeById(session.getUser().getId(), task.getId());
        Assert.assertNull(taskService.findById(session.getUser().getId(), task.getId()));
    }

    @Test(expected = EmptyIdException.class)
    public void removeByIdNull() {
        taskService.removeById(session.getUser().getId(), null);
        Assert.assertNotNull(taskService.findById(session.getUser().getId(), task.getId()));
    }

    @Test
    public void removeByIdIncorrect() {
        taskService.removeById(session.getUser().getId(), TEST_TASK_ID_INCORRECT);
        Assert.assertNotNull(taskService.findById(session.getUser().getId(), task.getId()));
    }

    @Test
    public void removeByIdIncorrectUser() {
        taskService.removeById(TEST_USER_ID_INCORRECT, this.task.getId());
        Assert.assertNotNull(taskService.findById(session.getUser().getId(), task.getId()));
    }

    @Test
    public void removeByName() {
        taskService.removeByName(session.getUser().getId(), TEST_TASK_NAME);
        Assert.assertNull(taskService.findById(session.getUser().getId(), task.getId()));
    }

    @Test
    public void removeByNameIncorrect() {
        taskService.removeByName(session.getUser().getId(), TEST_TASK_ID_INCORRECT);
        Assert.assertNotNull(taskService.findById(session.getUser().getId(), task.getId()));
    }

    @Test(expected = EmptyNameException.class)
    public void removeByNameNull() {
        taskService.removeByName(session.getUser().getId(), null);
        Assert.assertNotNull(taskService.findById(session.getUser().getId(), task.getId()));
    }

    @Test
    public void removeByNameIncorrectUser() {
        taskService.removeByName(TEST_USER_ID_INCORRECT, this.task.getName());
        Assert.assertNotNull(taskService.findById(session.getUser().getId(), task.getId()));
    }

    @Test
    public void startById() {
        @Nullable final Task task = taskService.startById(session.getUser().getId(), this.task.getId());
        Assert.assertNotNull(task);
        Assert.assertNotNull(task.getStatus());
        Assert.assertEquals(Status.IN_PROGRESS, task.getStatus());
    }

    @Test(expected = EmptyIdException.class)
    public void startByIdNull() {
        @Nullable final Task task = taskService.startById(session.getUser().getId(), null);
        Assert.assertNull(task);
    }

    @Test(expected = TaskNotFoundException.class)
    public void startByIdIncorrect() {
        @Nullable final Task task = taskService.startById(session.getUser().getId(), TEST_TASK_ID_INCORRECT);
        Assert.assertNull(task);
    }

    @Test(expected = TaskNotFoundException.class)
    public void startByIdIncorrectUser() {
        @Nullable final Task task = taskService.startById(TEST_USER_ID_INCORRECT, this.task.getId());
        Assert.assertNull(task);
    }

    @Test
    public void startByName() {
        @Nullable final Task task = taskService.startByName(session.getUser().getId(), TEST_TASK_NAME);
        Assert.assertNotNull(task);
        Assert.assertNotNull(task.getStatus());
        Assert.assertEquals(Status.IN_PROGRESS, task.getStatus());
    }

    @Test(expected = EmptyNameException.class)
    public void startByNameNull() {
        @Nullable final Task task = taskService.startByName(session.getUser().getId(), null);
        Assert.assertNull(task);
    }

    @Test(expected = TaskNotFoundException.class)
    public void startByNameIncorrect() {
        @Nullable final Task task = taskService.startByName(session.getUser().getId(), TEST_TASK_ID_INCORRECT);
        Assert.assertNull(task);
    }

    @Test(expected = TaskNotFoundException.class)
    public void startByNameIncorrectUser() {
        @Nullable final Task task = taskService.startByName(TEST_USER_ID_INCORRECT, TEST_TASK_NAME);
        Assert.assertNull(task);
    }

    @Test
    public void startByIndex() {
        @Nullable final Task task = taskService.startByIndex(session.getUser().getId(), 0);
        Assert.assertNotNull(task);
        Assert.assertNotNull(task.getStatus());
        Assert.assertEquals(Status.IN_PROGRESS, task.getStatus());
    }

    @Test(expected = TaskNotFoundException.class)
    public void startByIndexIncorrect() {
        @Nullable final Task task = taskService.startByIndex(session.getUser().getId(), 674);
        Assert.assertNull(task);
    }

    @Test(expected = TaskNotFoundException.class)
    public void startByIndexIncorrectUser() {
        @Nullable final Task task = taskService.startByIndex(TEST_USER_ID_INCORRECT, 674);
        Assert.assertNull(task);
    }

    @Test
    public void finishById() {
        @Nullable final Task task = taskService.finishById(session.getUser().getId(), this.task.getId());
        Assert.assertNotNull(task);
        Assert.assertNotNull(task.getStatus());
        Assert.assertEquals(Status.COMPLETED, task.getStatus());
    }

    @Test(expected = EmptyIdException.class)
    public void finishByIdNull() {
        @Nullable final Task task = taskService.finishById(session.getUser().getId(), null);
        Assert.assertNull(task);
    }

    @Test(expected = TaskNotFoundException.class)
    public void finishByIdIncorrect() {
        @Nullable final Task task = taskService.finishById(session.getUser().getId(), TEST_TASK_ID_INCORRECT);
        Assert.assertNull(task);
    }

    @Test(expected = TaskNotFoundException.class)
    public void finishByIdIncorrectUser() {
        @Nullable final Task task = taskService.finishById(TEST_USER_ID_INCORRECT, this.task.getId());
        Assert.assertNull(task);
    }

    @Test
    public void finishByName() {
        @Nullable final Task task = taskService.finishByName(session.getUser().getId(), TEST_TASK_NAME);
        Assert.assertNotNull(task);
        Assert.assertNotNull(task.getStatus());
        Assert.assertEquals(Status.COMPLETED, task.getStatus());
    }

    @Test(expected = EmptyNameException.class)
    public void finishByNameNull() {
        @Nullable final Task task = taskService.finishByName(session.getUser().getId(), null);
        Assert.assertNull(task);
    }

    @Test(expected = TaskNotFoundException.class)
    public void finishByNameIncorrect() {
        @Nullable final Task task = taskService.finishByName(session.getUser().getId(), TEST_TASK_ID_INCORRECT);
        Assert.assertNull(task);
    }

    @Test(expected = TaskNotFoundException.class)
    public void finishByNameIncorrectUser() {
        @Nullable final Task task = taskService.finishByName(TEST_USER_ID_INCORRECT, TEST_TASK_NAME);
        Assert.assertNull(task);
    }

    @Test
    public void finishByIndex() {
        @Nullable final Task task = taskService.finishByIndex(session.getUser().getId(), 0);
        Assert.assertNotNull(task);
        Assert.assertNotNull(task.getStatus());
        Assert.assertEquals(Status.COMPLETED, task.getStatus());
    }

    @Test(expected = TaskNotFoundException.class)
    public void finishByIndexIncorrect() {
        @Nullable final Task task = taskService.finishByIndex(session.getUser().getId(), 674);
        Assert.assertNull(task);
    }

    @Test(expected = TaskNotFoundException.class)
    public void finishByIndexIncorrectUser() {
        @Nullable final Task task = taskService.finishByIndex(TEST_USER_ID_INCORRECT, 674);
        Assert.assertNull(task);
    }

    @Test
    public void changeStatusById() {
        @Nullable final Task task = taskService.changeStatusById(session.getUser().getId(), this.task.getId(), Status.NOT_STARTED);
        Assert.assertNotNull(task);
        Assert.assertNotNull(task.getStatus());
        Assert.assertEquals(Status.NOT_STARTED, task.getStatus());
    }

    @Test
    public void changeStatusByName() {
        @Nullable final Task task = taskService.changeStatusByName(session.getUser().getId(), TEST_TASK_NAME, Status.NOT_STARTED);
        Assert.assertNotNull(task);
        Assert.assertNotNull(task.getStatus());
        Assert.assertEquals(Status.NOT_STARTED, task.getStatus());
    }

    @Test
    public void changeStatusByIndex() {
        @Nullable final Task task = taskService.changeStatusByIndex(session.getUser().getId(), 0, Status.NOT_STARTED);
        Assert.assertNotNull(task);
        Assert.assertNotNull(task.getStatus());
        Assert.assertEquals(Status.NOT_STARTED, task.getStatus());
    }

}