package ru.tsc.avramenko.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.tsc.avramenko.tm.dto.SessionDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;

public interface ISessionEndpoint {

    @WebMethod
    boolean closeSession(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session
    );

    @WebMethod
    SessionDTO openSession(
            @WebParam(name = "login", partName = "login") @NotNull String login,
            @WebParam(name = "password", partName = "password") @NotNull String password
    );

}