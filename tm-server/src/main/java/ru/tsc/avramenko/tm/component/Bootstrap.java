package ru.tsc.avramenko.tm.component;

import lombok.SneakyThrows;
import org.apache.activemq.broker.BrokerService;
import org.apache.activemq.usage.SystemUsage;
import org.apache.log4j.BasicConfigurator;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.avramenko.tm.api.service.*;
import ru.tsc.avramenko.tm.api.service.dto.*;
import ru.tsc.avramenko.tm.api.service.model.*;
import ru.tsc.avramenko.tm.endpoint.*;
import ru.tsc.avramenko.tm.service.*;
import ru.tsc.avramenko.tm.service.dto.*;
import ru.tsc.avramenko.tm.service.model.*;
import ru.tsc.avramenko.tm.util.SystemUtil;
import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

public final class Bootstrap implements IServiceLocator {

    @NotNull
    private final IServiceLocator serviceLocator = this;

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final ITaskDtoService taskDtoService = new TaskDtoService(connectionService);

    @NotNull
    private final IProjectDtoService projectDtoService = new ProjectDtoService(connectionService);

    @NotNull
    private final IProjectTaskDtoService projectTaskDtoService = new ProjectTaskDtoService(connectionService);

    @NotNull
    private final IUserDtoService userDtoService = new UserDtoService(connectionService, propertyService);

    @NotNull
    private final ISessionDtoService sessionDtoService = new SessionDtoService(connectionService, serviceLocator);

    @NotNull
    private final ILogService logService = new LogService();

    @NotNull
    private final IProjectService projectService = new ProjectService(connectionService);

    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(connectionService);

    @NotNull
    private final IDataService dataService = new DataService(serviceLocator);

    @NotNull
    private final IUserService userService = new UserService(connectionService, propertyService);

    @NotNull
    private final Backup backup = new Backup(this, propertyService, serviceLocator);

    @NotNull
    private final ISessionService sessionService = new SessionService(connectionService, serviceLocator);

    @NotNull
    private final SessionEndpoint sessionEndpoint = new SessionEndpoint(serviceLocator);

    @NotNull
    private final ITaskService taskService = new TaskService(connectionService);

    @NotNull
    private final UserEndpoint userEndpoint = new UserEndpoint(serviceLocator);

    @NotNull
    private final AdminUserEndpoint adminUserEndpoint = new AdminUserEndpoint(serviceLocator);

    @NotNull
    private final AdminDataEndpoint adminDataEndpoint = new AdminDataEndpoint(serviceLocator, backup);

    @NotNull
    private final ProjectEndpoint projectEndpoint = new ProjectEndpoint(serviceLocator);

    @NotNull
    private final TaskEndpoint taskEndpoint = new TaskEndpoint(serviceLocator);

    @NotNull final JmsMessageComponent jmc = new JmsMessageComponent();

    public void start(String[] args) {
        initJMSBroker();
        initPID();
        initEndpoint();
        backup.init();
    }

    @SneakyThrows
    private void initPID() {
            @NotNull final String filename = "task-manager.pid";
            @NotNull final String pid = Long.toString(SystemUtil.getPID());
            Files.write(Paths.get(filename), pid.getBytes());
            @NotNull final File file = new File(filename);
            file.deleteOnExit();
    }

    @SneakyThrows
    public void initJMSBroker() {
        BasicConfigurator.configure();
        @NotNull final BrokerService brokerService = new BrokerService();
        brokerService.addConnector("tcp://localhost:61616");
        brokerService.setPersistent(false);
        SystemUsage systemUsage = brokerService.getSystemUsage();
        systemUsage.getStoreUsage().setLimit(1024 * 1024 * 8);
        systemUsage.getTempUsage().setLimit(1024 * 1024 * 8);
        brokerService.start();
        jmc.run();
    }


    private void initEndpoint() {
        registry(sessionEndpoint);
        registry(userEndpoint);
        registry(adminUserEndpoint);
        registry(adminDataEndpoint);
        registry(projectEndpoint);
        registry(taskEndpoint);
    }

    private void registry(@Nullable final Object endpoint) {
        if (endpoint == null) return;
        @NotNull final String host = propertyService.getServerHost();
        final int port = propertyService.getServerPort();
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String wsdl = "http://" + host + "/" + port + "/" + name + "?wsdl";
        System.out.println(wsdl);
        Endpoint.publish(wsdl, endpoint);
    }

    @NotNull
    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @NotNull
    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @NotNull
    @Override
    public IProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    @NotNull
    @Override
    public IUserService getUserService() {
        return userService;
    }

    @NotNull
    @Override
    public IPropertyService getPropertyService() {
        return propertyService;
    }

    @NotNull
    @Override
    public ISessionService getSessionService() {
        return sessionService;
    }

    @Override
    public @NotNull IDataService getDataService() {
        return dataService;
    }

    @Override
    public @NotNull ITaskDtoService getTaskDtoService() {
        return taskDtoService;
    }

    @Override
    public @NotNull IProjectDtoService getProjectDtoService() {
        return projectDtoService;
    }

    @Override
    public @NotNull IProjectTaskDtoService getProjectTaskDtoService() {
        return projectTaskDtoService;
    }

    @Override
    public @NotNull IUserDtoService getUserDtoService() {
        return userDtoService;
    }

    @Override
    public @NotNull ISessionDtoService getSessionDtoService() {
        return sessionDtoService;
    }

}