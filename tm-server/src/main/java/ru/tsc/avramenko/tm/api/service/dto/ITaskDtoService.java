package ru.tsc.avramenko.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.avramenko.tm.dto.TaskDTO;
import ru.tsc.avramenko.tm.enumerated.Status;

import java.util.List;

public interface ITaskDtoService {

    void create(@Nullable String userId, @Nullable String name);

    void create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @Nullable
    TaskDTO changeStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

    @Nullable
    TaskDTO changeStatusByName(@Nullable String userId, @Nullable String name, @Nullable Status status);

    @Nullable
    TaskDTO changeStatusByIndex(@Nullable String userId, @Nullable Integer index, @Nullable Status status);

    @NotNull
    TaskDTO findByName(@Nullable String userId, @Nullable String name);

    @NotNull
    TaskDTO findByIndex(@Nullable String userId, @Nullable Integer index);

    @Nullable
    void removeByName(@Nullable String userId, @Nullable String name);

    @Nullable
    void removeByIndex(@Nullable String userId, @Nullable Integer index);

    @NotNull
    TaskDTO updateByIndex(@Nullable String userId, @Nullable Integer index, @Nullable String name, @Nullable String description);

    @Nullable
    TaskDTO updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description);

    @Nullable
    TaskDTO startById(@Nullable String userId, @Nullable String id);

    @Nullable
    TaskDTO startByName(@Nullable String userId, @Nullable String name);

    @Nullable
    TaskDTO startByIndex(@Nullable String userId, @Nullable Integer index);

    @Nullable
    TaskDTO finishById(@Nullable String userId, @Nullable String id);

    @Nullable
    TaskDTO finishByName(@Nullable String userId, @Nullable String name);

    @Nullable
    TaskDTO finishByIndex(@Nullable String userId, @Nullable Integer index);

    @NotNull
    TaskDTO findById(@Nullable String userId, @Nullable String id);

    @Nullable
    void removeById(@Nullable String userId, @Nullable String id);

    @Nullable
    List<TaskDTO> findAll(@Nullable String userId);

    @Nullable
    List<TaskDTO> findAll();

    void clear(@Nullable String userId);

    void clear();

    void addAll(@Nullable List<TaskDTO> tasks);

}