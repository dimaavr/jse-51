package ru.tsc.avramenko.tm.repository.model;

import org.hibernate.jpa.QueryHints;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.avramenko.tm.api.repository.model.ITaskRepository;
import ru.tsc.avramenko.tm.model.Task;
import javax.persistence.EntityManager;
import java.util.List;

public final class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

    public TaskRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @NotNull
    @Override
    public List<Task> findAll() {
        return entityManager
                .createQuery("SELECT e FROM Task e", Task.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .getResultList();
    }

    @Override
    public List<Task> findAllTaskByProjectId(String userId, String projectId) {
        return entityManager
                .createQuery("SELECT e FROM Task e WHERE e.user.id = :user_id AND e.project.id = :project_id", Task.class)
                .setParameter("user_id", userId)
                .setParameter("project_id", projectId)
                .getResultList();
    }

    @Nullable
    @Override
    public Task findByName(@NotNull final String userId, @Nullable final String name) {
        List<Task> list = entityManager
                .createQuery("SELECT e FROM Task e WHERE e.name = :name AND e.user.id = :user_id", Task.class)
                .setParameter("name", name)
                .setParameter("user_id", userId)
                .setMaxResults(1).getResultList();
        if (list.isEmpty())
            return null;
        else
            return list.get(0);
    }

    @Nullable
    @Override
    public Task findByIndex(@NotNull final String userId, final int index) {
        List<Task> list = entityManager
                .createQuery("SELECT e FROM Task e WHERE e.user.id = :user_id", Task.class)
                .setParameter("user_id", userId)
                .setFirstResult(index).setMaxResults(1).getResultList();
        if (list.isEmpty())
            return null;
        else
            return list.get(0);
    }

    @Override
    public void removeByName(@NotNull final String userId, @Nullable final String name) {
        entityManager
                .createQuery("DELETE FROM Task e WHERE e.name = :name AND e.user.id = :user_id")
                .setParameter("user_id", userId)
                .setParameter("name", name)
                .executeUpdate();
    }

    @Override
    public void removeByIndex(@NotNull final String userId, final int index) {
        entityManager
                .createQuery("DELETE FROM Task e WHERE e.user.id = :user_id")
                .setParameter("user_id", userId)
                .setFirstResult(index).setMaxResults(1).executeUpdate();
    }

    public void clear() {
        entityManager
                .createQuery("DELETE FROM Task e")
                .executeUpdate();
    }

    @Override
    public void clear(String userId) {
        entityManager
                .createQuery("DELETE FROM Task e WHERE e.user.id = :user_id")
                .setParameter("user_id", userId)
                .executeUpdate();
    }

    @Override
    public Task findById(String userId, String id) {
        List<Task> list = entityManager
                .createQuery("SELECT e FROM Task e WHERE e.id = :id AND e.user.id = :user_id", Task.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setParameter("id", id)
                .setParameter("user_id", userId)
                .setMaxResults(1).getResultList();
        if (list.isEmpty())
            return null;
        else
            return list.get(0);
    }

    @Override
    public void removeById(String userId, String id) {
        entityManager
                .createQuery("DELETE FROM Task e WHERE e.user.id = :user_id AND e.id = :id")
                .setParameter("user_id", userId)
                .setParameter("id", id)
                .executeUpdate();
    }

    @Override
    public List<Task> findAllById(String userId) {
        List<Task> list = entityManager
                .createQuery("SELECT e FROM Task e WHERE e.user.id = :user_id", Task.class)
                .setParameter("user_id", userId)
                .getResultList();
        if (list.isEmpty())
            return null;
        else
            return list;
    }

    @Override
    public void unbindAllTaskByProjectId(String userId, String projectId) {
        entityManager
                .createQuery("UPDATE Task e SET e.project.id = NULL WHERE e.user.id = :user_id AND e.project.id = :project_id")
                .setParameter("user_id", userId)
                .setParameter("project_id", projectId)
                .executeUpdate();
    }

}