package ru.tsc.avramenko.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.avramenko.tm.dto.SessionDTO;
import ru.tsc.avramenko.tm.enumerated.Role;

import java.util.List;

public interface ISessionDtoService {

    boolean checkDataAccess(@Nullable String login, @Nullable String password);

    @Nullable SessionDTO sign(@Nullable SessionDTO session);

    @Nullable
    void close(@NotNull SessionDTO session);

    @Nullable
    SessionDTO open(@Nullable String login, @Nullable String password);

    void validate(@NotNull SessionDTO session, @Nullable Role role);

    void validate(@Nullable SessionDTO session);

    @Nullable
    List<SessionDTO> findAll();

    @Nullable
    SessionDTO findById(@Nullable String id);

}